#!/bin/sh
set -eu

/usr/local/bin/dockerd-entrypoint.sh &

mkdir -p /etc/docker/config

# Copy /opa-config.yml.tmpl to /opa/config/config.yaml, but replace {{user}} with $NIRVATI_USER
sed "s/{{user}}/$NIRVATI_USER/g" /opa-config.yml.tmpl > /etc/docker/config/opa-config.yaml

until docker info > /dev/null 2>&1
do
  echo "Waiting for Docker to start..."
  sleep 1
done

docker plugin install --grant-all-permissions openpolicyagent/opa-docker-authz-v2:0.9 opa-args="-config-file /opa/config/opa-config.yaml" || true

cat > /etc/docker/daemon.json <<EOF
{
    "authorization-plugins": ["openpolicyagent/opa-docker-authz-v2:0.9"]
}
EOF

kill  -HUP $(ps aux | grep dockerd-entrypoint.sh | awk '{print $2}')
kill -HUP $(pidof dockerd) || true

exec /usr/local/bin/dockerd-entrypoint.sh
