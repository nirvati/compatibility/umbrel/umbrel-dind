FROM docker:dind

COPY entrypoint.sh /entrypoint.sh

COPY opa-config.yml.tmpl /opa-config.yml.tmpl

ENTRYPOINT [ "/entrypoint.sh" ]
